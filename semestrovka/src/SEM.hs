module SEM
       ( eval1,
        eval,
       Term (..),
       ) where

data Term = Variable String
            |Lambda String Term
            |Apply Term Term
            deriving(Eq)
instance Show Term where
    show (Variable v) = v
    show (Lambda v t) = "(\\" ++ show v ++ "." ++ show t ++ ")"
    show (Apply (Variable x) t) = "("++ show (Variable x) ++ " " ++ show t ++ ")"
    show (Apply t1 t2) = ""++ show t1 ++ " " ++ show t2 ++ ""

eval1 :: Term -> Maybe Term
eval1 (Apply (Lambda x y) t) = Just (repVariable x [] t y)
eval1 (Apply (Apply x y) m) = eval1 (Apply (eval $ Apply x y) m)
eval1 t = Just t

eval :: Term -> Term
eval x = case result of
  (Just x) -> x
  otherwise -> Variable "Not evaulated"
  where
    result = eval1 x


repVariable :: String -> [String] -> Term -> Term -> Term
repVariable x l t (Variable y) = if x==y then (renVariables l t) else  (Variable y)
repVariable x l t (Lambda y z) = Lambda y (repVariable x (y:l) t z)
repVariable x l t (Apply y z) = Apply (repVariable x l t y) (repVariable x l t z)

renameVariable :: [String] -> String -> Int -> String
renameVariable lst x n | elem (x ++ (show n)) lst = renameVariable lst x (n+1)
                  | otherwise = x ++ (show n)

renVariables :: [String] -> Term -> Term
renVariables lst (Lambda x term) | elem x lst = Lambda x (renVariables (filter (\el -> el /= x) lst) term)
                            | otherwise = Lambda x (renVariables lst term)
renVariables lst (Apply t1 t2) = Apply (renVariables lst t1) (renVariables lst t2)
renVariables lst (Variable x) | elem x lst = Variable (renameVariable lst x 0)
                    | otherwise = Variable x
